package loadInstance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * ajoute les instances au modele à partir d'un csv
 * 
 *
 */
public class csvTOturtle
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args)
	{
		try
		{
			final String MINECRAFT = "###  http://www.semanticweb.org/omontet/ontologies/2019/1/minecraft#";

			// file repertoire
			final String ONTO_DIRECTORY = "./resources/models/";

			// input file
			final String typeFile = ONTO_DIRECTORY + "type.csv";
			final String craftFile = ONTO_DIRECTORY + "craft.csv";
			final String dropFile = ONTO_DIRECTORY + "drop.csv";

			// output file
			final String out = ONTO_DIRECTORY + "instances.ttl";

			// create output file
			File fichier = new File(out);
			fichier.createNewFile();
			FileWriter writer = new FileWriter(fichier);

			HashSet<String> instance = new HashSet<String>();

			// Lecture du fichier type.csv et création de toute les instance
			System.out.println("\n\n");
			System.out.println("===============================");
			System.out.println("============= type ============");
			System.out.println("===============================");
			try
			{
				System.out.println(new java.io.File(".").getCanonicalPath() + "/" + typeFile);
				BufferedReader readFile = new BufferedReader(new FileReader(typeFile));
				String ligne = readFile.readLine();

				while (ligne != null && ligne.length() != 0)
				{
					String[] tab = ligne.split(",");
					if (!tab[0].equals("") && !tab[1].equals(""))
					{
						// Il y à obligatoirement l'objet et son type

						ArrayList<String> val = new ArrayList<String>();
						val.add(":" + tab[0] + " rdf:type owl:NamedIndividual ,:" + tab[1] + " .");
						instance.add(tab[0]);
						writer.write(MINECRAFT + tab[0] + "\n");
						for (String write : val)
						{
							writer.write("\t" + write + "\n");
						}
						writer.write("\n");
					}
					ligne = readFile.readLine();
				}
				readFile.close();
			} catch (IOException e)
			{
				System.err.println("Lecture fichier des type");
				e.printStackTrace();
			}

			// Lecture du fichier craft.csv et création des instances craft plus relation
			// associé
			System.out.println("\n\n");
			System.out.println("===============================");
			System.out.println("============= craft ===========");
			System.out.println("===============================");
			try
			{
				System.out.println(new java.io.File(".").getCanonicalPath() + "/" + craftFile);
				BufferedReader readFile = new BufferedReader(new FileReader(craftFile));
				String ligne = readFile.readLine();

				while (ligne != null && ligne.length() != 0)
				{
					String[] tab = ligne.split(",");
					if (!tab[0].equals("") && !tab[1].equals("") && !tab[2].equals(""))
					{ // Il y à obligatoirement l'object et un de ces éllément de craft avec quantité
						if (instance.contains(tab[0]))
						{
							System.out.print(tab[0] + " craft -> ");
							ArrayList<String> valCraft = new ArrayList<String>();
							// Création instance craft
							int i = 1;
							while (instance.contains("craft" + tab[0] + i))
							{
								i++;
							}
							String name = "craft" + tab[0] + i;
							valCraft.add(":" + name + " rdf:type owl:NamedIndividual ,:Craft .");
							valCraft.add(":" + name + " :CraftOf :" + tab[0] + " .");
							instance.add(name);
							System.out.println(name);

							HashMap<String, ArrayList<String>> blankNode = new HashMap<String, ArrayList<String>>();
							// Création des noeuds blancs de la recette
							i = 1;
							while (i < tab.length && !tab[i].equals(""))
							{
								// on test si l'element existe sinon on le dit à l'utilisateur
								if (!instance.contains(tab[i]))
								{
									System.err.println("\tl'instance " + tab[i] + " n'est pas définie !!!\n");
									i = i - 2;
									while (i > 0)
									{
										blankNode.remove(name + tab[i]);
										i = i - 2;
									}
									i = tab.length;
									instance.remove(name);
								} else
								{
									ArrayList<String> val = new ArrayList<String>();
									String node = name + tab[i];
									val.add(":" + node + " rdf:type owl:NamedIndividual .");
									val.add(":" + node + " rdf:object :" + tab[i] + " .");
									val.add(":" + node + " rdf:predicate :need .");
									val.add(":" + node + " rdf:subject :" + name + " .");
									val.add(":" + node + " :quantity " + tab[i + 1] + " .");

									blankNode.put(node, (ArrayList<String>) val.clone());
									i = i + 2;
								}
							}
							if (instance.contains(name))
							{
								writer.write(MINECRAFT + name + "\n");
								for (String write : valCraft)
								{
									writer.write("\t" + write + "\n");
								}
								writer.write("\n");
								for (String key : blankNode.keySet())
								{
									writer.write(MINECRAFT + key + "\n");
									for (String write : blankNode.get(key))
									{
										writer.write("\t" + write + "\n");
									}
									writer.write("\n");
								}
							}
						} else
						{
							System.err.println(tab[0] + " n'est pas définie on ne peu pas créer sa recette");
						}
					}
					ligne = readFile.readLine();
				}
				readFile.close();
			} catch (IOException e)
			{
				System.err.println("Lecture fichier des craft");
				e.printStackTrace();
			}

			// Lecture du fichier drop.csv et création des instances drop plus relation
			// associé
			System.out.println("\n\n");
			System.out.println("===============================");
			System.out.println("============= drop ============");
			System.out.println("===============================");
			try
			{
				System.out.println(new java.io.File(".").getCanonicalPath() + "/" + dropFile);
				BufferedReader readFile = new BufferedReader(new FileReader(dropFile));
				String ligne = readFile.readLine();

				while (ligne != null && ligne.length() != 0)
				{
					String[] tab = ligne.split(",");
					if (!tab[0].equals("") && !tab[1].equals(""))
					{ // Il y à obligatoirement l'object et sa source
						if (instance.contains(tab[0]))
						{
							System.out.print(tab[0] + " drop -> ");
							ArrayList<String> valDrop = new ArrayList<String>();
							// Création instance drop
							int i = 1;
							while (instance.contains("drop" + tab[0] + i))
							{
								i++;
							}
							String name = "drop" + tab[0] + i;
							valDrop.add(":" + name + " rdf:type owl:NamedIndividual ,:Drop .");
							valDrop.add(":" + name + " :DropOf :" + tab[0] + " .");
							instance.add(name);
							System.out.println(name);

							HashMap<String, ArrayList<String>> blankNode = new HashMap<String, ArrayList<String>>();

							// Création noeud blanc pour la source
							ArrayList<String> val = new ArrayList<String>();
							String sourceNode = name + tab[1];
							val.add(":" + sourceNode + " rdf:type owl:NamedIndividual ,owl:Thing .");
							val.add(":" + sourceNode + " rdf:object :" + tab[1] + " .");
							val.add(":" + sourceNode + " rdf:predicate :source .");
							val.add(":" + sourceNode + " rdf:subject :" + name + " .");
							blankNode.put(sourceNode, (ArrayList<String>) val.clone());

							// Création des noeuds blancs des outil
							i = 2;
							while (i < tab.length && !tab[i].equals(""))
							{
								val = new ArrayList<String>();
								sourceNode = name + tab[i];
								val.add(":" + sourceNode + " rdf:type owl:NamedIndividual ,owl:Thing .");
								val.add(":" + sourceNode + " rdf:object :" + tab[i] + " .");
								val.add(":" + sourceNode + " rdf:predicate :tool .");
								val.add(":" + sourceNode + " rdf:subject :" + name + " .");
								blankNode.put(sourceNode, (ArrayList<String>) val.clone());
								i++;
							}
							if (instance.contains(name))
							{
								writer.write(MINECRAFT + name + "\n");
								for (String write : valDrop)
								{
									writer.write("\t" + write + "\n");
								}
								writer.write("\n");
								for (String key : blankNode.keySet())
								{
									writer.write(MINECRAFT + key + "\n");
									for (String write : blankNode.get(key))
									{
										writer.write("\t" + write + "\n");
									}
									writer.write("\n");
								}
							}
						} else
						{
							System.err.println(tab[0] + " n'est pas définie on ne peu pas créer son drop");
						}
					}
					ligne = readFile.readLine();
				}
				readFile.close();
			} catch (IOException e)
			{
				System.err.println("Lecture fichier des craft");
				e.printStackTrace();
			}

			System.out.println(instance.toString());
			writer.close();

		} catch (Exception e)
		{
			System.err.println("Impossible de creer le fichier");
			return;
		}
	}
}
